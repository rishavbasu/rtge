#pragma once

#include "core/types.h"
#include <math.h>

#define RTGE_REAL_EPSILON 0.000001f

RTGE_INLINE rtge_real rtge_abs(rtge_real r)
{
	if (r < 0) return -r;
	return r;
}

RTGE_INLINE rtge_real rtge_sqr(rtge_real r)
{
	return r * r;
}

RTGE_INLINE rtge_real rtge_sqrt(rtge_real r)
{
	return sqrt(r);
}


RTGE_INLINE rtge_bool rtge_is_equal_to(rtge_real a, rtge_real b, rtge_real e = RTGE_REAL_EPSILON)
{
	return rtge_abs(a - b) < e;
}

RTGE_INLINE rtge_bool rtge_is_equal_to(rtge_vec2 a, rtge_vec2 b, rtge_real e = RTGE_REAL_EPSILON)
{
	return rtge_abs(a.x - b.x) < e && rtge_abs(a.y - b.y) < e;
}

RTGE_INLINE rtge_bool rtge_is_zero(rtge_real a, rtge_real e = RTGE_REAL_EPSILON)
{
	return a < e;
}

// Returns the sign of a number.
// For example rtge_sign(3.425f) = 1, rtge_sign(-2.56) = -1, and rtge_sign(0.0f) = 0.
RTGE_INLINE rtge_int rtge_sign(rtge_real r)
{
	if (r > 0.0f)
		return 1;
	if (r < 0.0f)
		return -1;
	return 0;
}

RTGE_INLINE rtge_real rtge_min(rtge_real a, rtge_real b)
{
	if (a > b)
		return b;

	return a;
}

RTGE_INLINE rtge_real rtge_max(rtge_real a, rtge_real b)
{
	if (a > b)
		return b;

	return a;
}

// Returns a vector with component-wise max.
// rtge_max((1, -5), (-3, 2)) = (1, 2)
RTGE_INLINE rtge_vec2 rtge_max(rtge_vec2 a, rtge_vec2 b)
{
	return rtge_vec2(rtge_max(a.x, b.x), rtge_max(a.y, b.y));
}

// Returns the number that is closest to zero (not the minimum). 
// For example rtge_unsigned_min(-2, 3) returns -2 and rtge_unsigned_min(-3, 2) returns 2.
RTGE_INLINE rtge_real rtge_closest_to_zero(rtge_real a, rtge_real b)
{

}

RTGE_INLINE rtge_real rtge_sqr_dist(rtge_vec2 a, rtge_vec2 b)
{
	return rtge_sqr(a.x - b.x) + rtge_sqr(a.y - b.y);
}

RTGE_INLINE rtge_real rtge_sqr_magnitude(rtge_vec2 v)
{
	return v.x * v.x + v.y * v.y;
}

RTGE_INLINE rtge_vec2 rtge_vec2_normalized(rtge_vec2 v)
{
	return v / rtge_sqrt(rtge_sqr_magnitude(v));
}

RTGE_INLINE rtge_vec2 rtge_vec2_normalized_safe(rtge_vec2 v, rtge_real e = RTGE_REAL_EPSILON)
{
	rtge_real mag = rtge_sqr_magnitude(v);
	if (rtge_is_zero(mag, e))
	{
		return rtge_vec2(0.0f, 0.0f);
	}

	return v / rtge_sqrt(mag);
}





RTGE_INLINE rtge_bool rtge_make_rect_min_max(rtge_vec2 min, rtge_vec2 max, rtge_rect* rect)
{
	if (max.x < min.x) return false;
	if (max.y < min.y) return false;

	rect->max = max;
	rect->min = min;
	return true;
}


RTGE_INLINE rtge_bool rtge_make_rect_center_halfextents(rtge_vec2 center, rtge_vec2 halfextents, rtge_rect* rect)
{
	if (halfextents.x < 0.0f) return false;
	if (halfextents.y < 0.0f) return false;

	rect->max = center + halfextents;
	rect->min = center - halfextents;
	return true;
}

RTGE_INLINE rtge_bool rtge_make_circle_center_radius(rtge_vec2 center, rtge_real radius, rtge_circle* circle)
{
	if (radius < 0.0f) return false;

	circle->center = center;
	circle->radius = radius;
	return true;
}



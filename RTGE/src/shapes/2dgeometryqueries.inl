

RTGE_INLINE rtge_real rtge_signed_distance_point_rect(rtge_vec2 p, rtge_rect* r)
{
	rtge_vec2 diff = rtge_max(p - r->min, r->max - p);

	return rtge_sqr_magnitude(rtge_max(rtge_vec2(0.0f), diff)) + rtge_min(0.0f, rtge_max(diff.x, diff.y));
}

RTGE_INLINE rtge_real rtge_sqr_distance_rect_rect(rtge_rect* a, rtge_rect* b)
{
	rtge_bool within_x_range = (a->min.x <= b->max.x && a->max.x >= b->min.x);
	rtge_bool within_y_range = (a->min.y <= b->max.y && a->max.y >= b->min.y);
	rtge_real x_dist = rtge_min(rtge_abs(a->min.x - b->max.x), rtge_abs(a->max.x - b->min.x));
	rtge_real y_dist = rtge_min(rtge_abs(a->min.y - b->max.y), rtge_abs(a->max.y - b->min.y));
	if (within_x_range)
		x_dist = 0.0f;
	if (within_y_range)
		y_dist = 0.0f;

	return x_dist * x_dist + y_dist * y_dist;
}

RTGE_INLINE rtge_bool rtge_overlap_rect_rect(rtge_rect* a, rtge_rect* b)
{
	rtge_bool within_x_range = (a->min.x <= b->max.x && a->max.x >= b->min.x);
	rtge_bool within_y_range = (a->min.y <= b->max.y && a->max.y >= b->min.y);
	return within_x_range && within_y_range;
}

RTGE_INLINE rtge_bool rtge_intersection_rect_rect(rtge_rect* a, rtge_rect* b, rtge_2d_hit_info* out_info)
{
	if (!rtge_overlap_rect_rect(a, b))
	{
		out_info->hit = false;
		return false;
	}

	out_info->hit = true;
	rtge_vec2 a_center = (a->min + a->max) / 2.0f;
	rtge_vec2 b_center = (b->min + b->max) / 2.0f;
	
	rtge_real x_penetration = a_center.x < b_center.x ? b->min.x - a->max.x : a->min.x - b->max.x;
	rtge_real y_penetration = a_center.y < b_center.y ? b->min.y - a->max.y : a->min.y - b->max.y;
	if (rtge_abs(x_penetration) < rtge_abs(y_penetration))
	{
		out_info->normal = rtge_vec2(1.0f, 0.0f) * rtge_sign(x_penetration); 
		out_info->penetration = rtge_abs(x_penetration);
	}
	else
	{
		out_info->normal = rtge_vec2(0.0f, 1.0f) * rtge_sign(y_penetration); 
		out_info->penetration = rtge_abs(y_penetration);
	}
	return true;
}

// TODO: Could this be branchless?
RTGE_INLINE rtge_real rtge_signed_distance_circle_circle(rtge_circle* a, rtge_circle* b)
{
	rtge_real center_dist = rtge_sqrt(rtge_sqr_dist(a->center, b->center));
	if (center_dist > a->radius + b->radius)
		return center_dist - (a->radius + b->radius);
	if (center_dist > rtge_abs(b->radius - a->radius))
		return 0.0f;

	return center_dist - rtge_abs(a->radius - b->radius);
}

RTGE_INLINE rtge_bool rtge_overlap_circle_circle(rtge_circle* a, rtge_circle* b)
{
	return rtge_sqr_dist(a->center, b->center) < rtge_sqr(a->radius + b->radius);
}

RTGE_INLINE rtge_bool rtge_intersection_circle_circle(rtge_circle* a, rtge_circle* b, rtge_2d_hit_info* out_info)
{
	rtge_real center_dist = rtge_sqr_dist(a->center, b->center);
	if (center_dist > rtge_sqr(a->radius + b->radius))
	{
		out_info->hit = false;
		return false;
	}
	out_info->hit = true;
	if (rtge_is_zero(center_dist))
	{
		out_info->normal = rtge_vec2(1.0f, 0.0f);
	}
	else
	{
		out_info->normal = rtge_vec2_normalized(a->center - b->center);
	}
	
	out_info->penetration = rtge_abs((sqrt(center_dist) - a->radius) - b->radius);
	return true;
}

// TODO: Is this possible to make branchless?
RTGE_INLINE rtge_real rtge_signed_distance_circle_rect(rtge_circle* c, rtge_rect* r)
{
	rtge_real center_dist = rtge_signed_distance_point_rect(c->center, r);
	
	if (rtge_abs(center_dist) <= c->radius)
	{
		return 0.0f;
	}
	if (center_dist < 0.0f)
	{
		return center_dist + c->radius;
	}
	return center_dist - c->radius;
}

RTGE_INLINE rtge_bool rtge_overlap_circle_rect(rtge_circle* c, rtge_rect* r)
{
	rtge_real center_dist = rtge_signed_distance_point_rect(c->center, r);
	return center_dist <= c->radius;
}

RTGE_INLINE rtge_bool rtge_intersection_circle_rect(struct rtge_circle* c, struct rtge_rect* r, struct rtge_2d_hit_info* out_info)
{
	rtge_vec2 diff = rtge_max(c->center - r->min, r->max - c->center);
	rtge_real center_dist = rtge_sqr_magnitude(rtge_max(rtge_vec2(0.0f), diff)) + rtge_min(0.0f, rtge_max(diff.x, diff.y));

	if (center_dist > c->radius)
	{
		out_info->hit = false;
		return false;
	}
	
	out_info->penetration = center_dist - c->radius;
	//out_info->normal = 
	return true;
}








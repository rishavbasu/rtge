#pragma once

#include "core/types.h"

// Queries include: distance, overlap, penetration
// 
// Distance - Returns the distance between the 2 closest points on 2 shapes.
// Overlap - Returns true or false depending on if 2 shapes share a point in common
// Intersection - Returns a more detailed intersection test between 2 shapes with their intersection point,
//				 penetration distance, and normal vector


struct rtge_2d_hit_info
{
	rtge_vec2 normal = rtge_vec2(0.0f);		// The normal of the intersection
	rtge_bool hit = false;					// Did the shapes intersect?
	rtge_real penetration = 0.0f;			// The distance the two shapes are penetrating
};

RTGE_INLINE rtge_real rtge_signed_distance_point_rect(rtge_vec2 p, struct rtge_rect* r);

// Returns the distance between two rectangles (0 when intersecting)
RTGE_INLINE rtge_real rtge_sqr_distance_rect_rect(struct rtge_rect* a, struct rtge_rect* b);

RTGE_INLINE rtge_bool rtge_overlap_rect_rect(struct rtge_rect* a, struct rtge_rect* b);

// Returns if the two rectangles intersected and proved more detailed information about the intersection
RTGE_INLINE rtge_bool rtge_intersection_rect_rect(struct rtge_rect* a, struct rtge_rect* b, struct rtge_2d_hit_info* out_info);

RTGE_INLINE rtge_real rtge_signed_distance_circle_circle(struct rtge_circle* a, struct rtge_circle* b);

RTGE_INLINE rtge_bool rtge_overlap_circle_circle(struct rtge_circle* a, struct rtge_circle* b);

RTGE_INLINE rtge_bool rtge_intersection_circle_circle(struct rtge_circle* a, struct rtge_circle* b, struct rtge_2d_hit_info* out_info);

RTGE_INLINE rtge_real rtge_signed_distance_circle_rect(struct rtge_circle* c, struct rtge_rect* r);

RTGE_INLINE rtge_bool rtge_overlap_circle_rect(struct rtge_circle* c, struct rtge_rect* r);

RTGE_INLINE rtge_bool rtge_intersection_circle_rect(struct rtge_circle* c, struct rtge_rect* r, struct rtge_2d_hit_info* out_info);


#include "2dgeometryqueries.inl"

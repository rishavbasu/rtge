#pragma once

#include "core/types.h"

struct rtge_rect
{
	rtge_vec2 min;
	rtge_vec2 max;
};

struct rtge_circle
{
	rtge_vec2 center;
	rtge_real radius;
};

RTGE_INLINE rtge_bool rtge_make_rect_min_max(rtge_vec2 min, rtge_vec2 max, rtge_rect* rect);

RTGE_INLINE rtge_bool rtge_make_rect_center_halfextents(rtge_vec2 center, rtge_vec2 halfextents, rtge_rect* rect);

RTGE_INLINE rtge_bool rtge_make_circle_center_radius(rtge_vec2 center, rtge_real radius, rtge_circle* circle);

#include "2dshapes.inl"

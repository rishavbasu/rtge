#pragma once

#define RTGE_INLINE inline


typedef unsigned int rtge_bool;
#define RTGE_FALSE 0
#define RTGE_TRUE 1

typedef signed int rtge_int;

#ifndef RTGE_DOUBLE_PRECISION
typedef float rtge_real;
#else
typedef double rtge_real;
#endif

struct rtge_vec2
{
	rtge_vec2();
	rtge_vec2(rtge_real r);
	rtge_vec2(rtge_real x, rtge_real y);

	rtge_real x;
	rtge_real y;
};

rtge_vec2::rtge_vec2()
	: x(0.0f), y(0.0f)
{}

rtge_vec2::rtge_vec2(rtge_real r)
	: x(r), y(r)
{}

rtge_vec2::rtge_vec2(rtge_real x_val, rtge_real y_val)
	: x(x_val), y(y_val)
{}

RTGE_INLINE rtge_vec2 operator+(rtge_vec2 a, rtge_vec2 b)
{
	rtge_vec2 result;

	result.x = a.x + b.x;
	result.y = a.y + b.y;
	return result;
}

RTGE_INLINE rtge_vec2 operator-(rtge_vec2 v)
{
	rtge_vec2 result(-v.x, -v.y);
	return v;
}

RTGE_INLINE rtge_vec2 operator-(rtge_vec2 a, rtge_vec2 b)
{
	rtge_vec2 result;

	result.x = a.x + -b.x;
	result.y = a.y + -b.y;
	return result;
}


RTGE_INLINE rtge_vec2 operator*(rtge_vec2 v, rtge_real f)
{
	rtge_vec2 result;

	result.x = v.x * f;
	result.y = v.y * f;
	return result;
}

RTGE_INLINE rtge_vec2 operator*(rtge_real f, rtge_vec2 v)
{
	rtge_vec2 result;

	result.x = v.x * f;
	result.y = v.y * f;
	return result;
}

RTGE_INLINE rtge_vec2 operator/(rtge_vec2 v, rtge_real f)
{
	rtge_vec2 result;

	result.x = v.x / f;
	result.y = v.y / f;
	return result;
}


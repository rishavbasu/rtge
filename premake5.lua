workspace "RTGE"
    architecture "x64"
    startproject "Examples"

    configurations
    {
        "Debug",
        "Release",
        "Dist"
    }

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "RTGE"
    location "RTGE"
    kind "StaticLib"
    language "C++"
    cppdialect "C++11"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp",
        "%{prj.name}/include/**.h",
    }

    includedirs
    {
        "%{prj.name}/src/"
    }

    filter "system:windows"
        systemversion "latest"

    filter "configurations:Debug"
        defines "RTGE_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "RTGE_RELEASE"
        runtime "Release"
        optimize "on"

    filter "configurations:Dist"
        defines "RTGE_DIST"
        runtime "Release"
        optimize "on"

project "Examples"
    location "examples"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++11"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/**.h",
        "%{prj.name}/**.cpp"
    }

    includedirs
    {
        "RTGE/src/",
        "RTGE/include/",
        "Examples/"
    }

    links
    {
        "RTGE"
    }

    filter "system:windows"
        systemversion "latest"

        defines 
        {
            "RTGE_PLATFORM_WINDOWS"
        }

    filter "configurations:Debug"
        defines "CK_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "CK_RELEASE"
        runtime "Release"
        optimize "on"

    filter "configurations:Dist"
        defines "CK_DIST"
        runtime "Release"
        optimize "on"

project "Testing"
    location "testing"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++11"
    staticruntime "on"
    
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")

    files
    {
        "%{prj.name}/**.h",
        "%{prj.name}/**.cpp"
    }

    includedirs
    {
        "RTGE/src/",
        "RTGE/include/",
        "Testing/"
    }

    links
    {
        "RTGE"
    }

    filter "system:windows"
        systemversion "latest"

        defines 
        {
            "RTGE_PLATFORM_WINDOWS"
        }

    filter "configurations:Debug"
        defines "CK_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "CK_RELEASE"
        runtime "Release"
        optimize "on"

    filter "configurations:Dist"
        defines "CK_DIST"
        runtime "Release"
        optimize "on"
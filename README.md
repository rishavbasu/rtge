# RTGE (RealTimeGeometryEngine)

## Project Details
This project serves as a small library for doing geometry queries like "are these 2 rectangles colliding" with an emphasis on speed to be used in real time applications. This project currently isn't being worked on and it only supports circles and rectangles collision queries.

#pragma once

#include <stdio.h>
#include "core/types.h"

#define TEST(t, test) run_test(t, test, __LINE__, __FILE__)

struct tester
{
	unsigned int test_num = 0;
};

void run_test(tester* t, unsigned int test, int line, const char* filename)
{
	if (test)
	{
		printf("Test %d passed!\n", t->test_num++);
	}
	else
	{
		printf("Test %d failed on line %d in %s!\n", t->test_num++, line, filename);
	}
}

const char* bool_str(bool b)
{
	return b ? "True" : "False";
}

char* vec2_str(rtge_vec2 v)
{
	//(v.x, v.y)
	const int digits = 6;
	char* c = new char[50];
	snprintf(c, 50, "(%.6f, %.6f)", v.x, v.y);
	//snprintf(c, 50, "(0.0, 0.0)");
	return c;
}

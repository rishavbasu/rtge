#pragma once

#include "tests/tester.h"
#include "2dgeometry.h"
#include <string>

struct rect_collision_results
{
	rect_collision_results(rtge_bool overlap,
	rtge_real sqr_dist,
	rtge_vec2 normal,
	rtge_real penetration)
		: overlap(overlap), sqr_dist(sqr_dist), normal(normal), penetration(penetration) {}

	rect_collision_results(rtge_bool overlap,
	rtge_real sqr_dist)
		: overlap(overlap), sqr_dist(sqr_dist) {}


	rtge_bool overlap;
	rtge_real sqr_dist;
	rtge_vec2 normal;
	rtge_real penetration;
};

bool test_rect_pair(rtge_vec2 a_min, rtge_vec2 a_max, rtge_vec2 b_min, rtge_vec2 b_max, rect_collision_results* results);

void run_rect_collision_tests(tester* t)
{
	rect_collision_results results1(1, 0.0f, { 0.0f, -1.0f }, 0.5f);
	TEST(t, test_rect_pair({ 0.0f, 0.0f }, { 1.0f, 1.0f }, { 0.5f, 0.5f }, { 1.5f, 1.5f }, &results1));

	rect_collision_results results2(0, 0.5f);
	TEST(t, test_rect_pair({ 0.0f, 0.0f }, { 1.0f, 1.0f }, { 1.5f, 1.5f }, { 2.5f, 2.5f }, &results2));

	rect_collision_results results3(1, 0.0f, { 0.0f, -1.0f }, 2.0f);
	TEST(t, test_rect_pair({ -1.0f, -1.0f }, { 1.0f, 1.0f }, { -1.0f, -1.0f }, { 1.0f, 1.0f }, &results3));

	rect_collision_results results4(1, 0.0f, { 0.0f, -1.0f }, 2.0f);
	TEST(t, test_rect_pair({ -1.5f, -1.5f }, { 1.0f, 1.0f }, { -1.0f, -1.0f }, { 1.5f, 1.5f }, &results4));

	rect_collision_results results5(1, 0.0f, { 0.0f, -1.0f }, 2.0f);
	TEST(t, test_rect_pair({ -1.0f, -1.0f }, { 1.5f, 1.5f }, { -1.5f, -1.5f }, { 1.0f, 1.0f }, &results5));

	rect_collision_results results6(1, 0.0f, { 0.0f, -1.0f }, 6.0f);
	TEST(t, test_rect_pair({ -5.0f, -5.0f }, { 5.0f, 5.0f }, { -1.0f, -1.0f }, { 1.0f, 1.0f }, &results6));

	rect_collision_results results7(1, 0.0f, { 0.0f, -1.0f }, 6.0f);
	TEST(t, test_rect_pair({ -1.0f, -1.0f }, { 1.0f, 1.0f }, { -5.0f, -5.0f }, { 5.0f, 5.0f }, &results7));
}

bool test_rect_pair(rtge_vec2 a_min, rtge_vec2 a_max, rtge_vec2 b_min, rtge_vec2 b_max, rect_collision_results* results)
{
	rtge_rect a, b = rtge_rect();
	rtge_make_rect_min_max(a_min, a_max, &a);
	rtge_make_rect_min_max(b_min, b_max, &b);

	rtge_2d_hit_info info;
	rtge_real sqr_dist = rtge_sqr_distance_rect_rect(&a, &b);
	bool dist = rtge_is_equal_to(sqr_dist, results->sqr_dist);
	if (!dist) printf("Distance: actual: %f, expected: %f\n", sqr_dist, results->sqr_dist);
	bool overlap = rtge_overlap_rect_rect(&a, &b) == results->overlap &&
		rtge_intersection_rect_rect(&a, &b, &info) == results->overlap &&
		info.hit == results->overlap;
	if (!overlap) printf("Overlap: actual: %s, expected: %s\n", bool_str(info.hit), bool_str(results->overlap));

	bool norm = true;
	bool penetration = true;
	if (info.hit)
	{
		norm = rtge_is_equal_to(info.normal, results->normal);
		penetration = rtge_is_equal_to(info.penetration, results->penetration);
	}
	if (!norm) printf("Normal: actual: %s, expected: %s\n", vec2_str(info.normal), vec2_str(results->normal));
	if (!penetration) printf("Penetration: actual: %f, expected: %f\n", info.penetration, results->penetration);
	return dist && overlap && norm && penetration;
}

#pragma once

#include "tests/tester.h"
#include "2dgeometry.h"
#include <string>
#include <iostream>

struct circle_collision_results
{
	circle_collision_results(rtge_bool overlap,
	rtge_real sqr_dist,
	rtge_vec2 normal,
	rtge_real penetration)
		: overlap(overlap), sqr_dist(sqr_dist), normal(normal), penetration(penetration) {}

	circle_collision_results(rtge_bool overlap,
	rtge_real sqr_dist)
		: overlap(overlap), sqr_dist(sqr_dist) {}


	rtge_bool overlap;
	rtge_real sqr_dist;
	rtge_vec2 normal;
	rtge_real penetration;
};

bool test_circle_pair(rtge_vec2 a_center, rtge_real a_radius, rtge_vec2 b_center, rtge_real b_radius, circle_collision_results* results);

void run_circle_collision_tests(tester* t)
{
	circle_collision_results results1(0, 3.0f);
	TEST(t, test_circle_pair({ 0.0f, 0.0f }, 1.0f, { 5.0f, 0.0f }, 1.0f, &results1));

	circle_collision_results results2(0, 3.0f);
	TEST(t, test_circle_pair({ 0.0f, 0.0f }, 1.0f, {-5.0f, 0.0f }, 1.0f, &results2));

	circle_collision_results results3(1, 0.0f, { -1.0f, 0.0f }, 1.0f);
	TEST(t, test_circle_pair({ -2.0f, 0.0f }, 2.5f, { 2.0f, 0.0f }, 2.5f, &results3));

	circle_collision_results results4(1, 0.0f, { 1.0f, 0.0f }, 1.0f);
	TEST(t, test_circle_pair({ 2.0f, 0.0f }, 2.5f, { -2.0f, 0.0f }, 2.5f, &results4));

	circle_collision_results results5(1, 0.0f, { 1.0f, 0.0f }, 2.0f);
	TEST(t, test_circle_pair({ 0.0f, 0.0f }, 1.0f, { 0.0f, 0.0f }, 1.0f, &results5));

	circle_collision_results results6(1, -3.0f, { 0.0f, -1.0f }, 5.0f);
	TEST(t, test_circle_pair({ 0.0f, 0.0f }, 5.0f, { 0.0f, 1.0f }, 1.0f, &results6));

	circle_collision_results results7(1, -3.0f, { 0.0f, -1.0f }, 5.0f);
	TEST(t, test_circle_pair({ 0.0f, 0.0f }, 1.0f, { 0.0f, 1.0f }, 5.0f, &results7));
}

bool test_circle_pair(rtge_vec2 a_center, rtge_real a_radius, rtge_vec2 b_center, rtge_real b_radius, circle_collision_results* results)
{
	rtge_circle a, b;
	rtge_make_circle_center_radius(a_center, a_radius, &a);
	rtge_make_circle_center_radius(b_center, b_radius, &b);

	rtge_2d_hit_info info;
	rtge_real sqr_dist = rtge_signed_distance_circle_circle(&a, &b);
	bool dist = rtge_is_equal_to(sqr_dist, results->sqr_dist);
	if (!dist) printf("Distance: actual: %f, expected: %f\n", sqr_dist, results->sqr_dist);
	bool overlap = rtge_overlap_circle_circle(&a, &b) == results->overlap &&
		rtge_intersection_circle_circle(&a, &b, &info) == results->overlap &&
		info.hit == results->overlap;
	if (!overlap) printf("Overlap: actual: %s, expected: %s\n", bool_str(info.hit), bool_str(results->overlap));

	bool norm = true;
	bool penetration = true;
	if (info.hit)
	{
		norm = rtge_is_equal_to(info.normal, results->normal);
		penetration = rtge_is_equal_to(info.penetration, results->penetration);
	}
	if (!norm) printf("Normal: actual: %s, expected: %s\n", vec2_str(info.normal), vec2_str(results->normal));
	if (!penetration) printf("Penetration: actual: %f, expected: %f\n", info.penetration, results->penetration);
	return dist && overlap && norm && penetration;
}




#include "tests/tester.h"
#include "tests/rectangle_collision_tests/rectangle_collision_tests.h"
#include "tests/circle_collision_tests/circle_collision_tests.h"

int main(int argc, const char* argv[])
{
	
	tester t = tester();
	run_rect_collision_tests(&t);
	run_circle_collision_tests(&t);
}
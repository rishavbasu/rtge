
#include "00_helloworld/helloworld.h"
#include "01_rectangles/rectangles_example.h"

int main(int argc, const char* argv[])
{
	hello_world_example hello_world = hello_world_example();
	hello_world.run_example();
	
	rectangles_example rectangles = rectangles_example();
	rectangles.run_example();


}
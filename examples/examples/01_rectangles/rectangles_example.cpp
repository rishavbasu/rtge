#include "rectangles_example.h"
#include "2dgeometry.h"
#include <string>
#include <sstream>
#include <stdio.h>

void test_rect_pair(rtge_vec2 a_min, rtge_vec2 a_max, rtge_vec2 b_min, rtge_vec2 b_max);

void rectangles_example::run_example()
{
	test_rect_pair({ 0.0f, 0.0f }, { 1.0f, 1.0f }, { 0.5f, 0.5f }, { 1.5f, 1.5f });
	test_rect_pair({ 0.0f, 0.0f }, { 1.0f, 1.0f }, { 1.5f, 1.5f }, { 2.5f, 2.5f });
	test_rect_pair({ -1.0f, -1.0f }, { 1.0f, 1.0f }, { -1.0f, -1.0f }, { 1.0f, 1.0f });
	test_rect_pair({ -1.5f, -1.5f }, { 1.0f, 1.0f }, { -1.0f, -1.0f }, { 1.5f, 1.5f });
	test_rect_pair({ -1.0f, -1.0f }, { 1.5f, 1.5f }, { -1.5f, -1.5f }, { 1.0f, 1.0f });
	test_rect_pair({ -5.0f, -5.0f }, { 5.0f, 5.0f }, { -1.0f, -1.0f }, { 1.0f, 1.0f });
	test_rect_pair({ -1.0f, -1.0f }, { 1.0f, 1.0f }, { -5.0f, -5.0f }, { 5.0f, 5.0f });
}

std::string vec2_as_string(rtge_vec2 v)
{
	return "(" + std::to_string(v.x) + ", " + std::to_string(v.y) + ")";
}

void test_rect_pair(rtge_vec2 a_min, rtge_vec2 a_max, rtge_vec2 b_min, rtge_vec2 b_max)
{
	rtge_rect a, b = rtge_rect();
	rtge_make_rect_min_max(a_min, a_max, &a);
	rtge_make_rect_min_max(b_min, b_max, &b);

	printf("Creating rectangles A: min = %s, max = %s, and B: min = %s, max = %s\n",
		vec2_as_string(a_min).c_str(),
		vec2_as_string(a_max).c_str(),
		vec2_as_string(b_min).c_str(),
		vec2_as_string(b_max).c_str());

	printf("Distance: %f\n", rtge_sqr_distance_rect_rect(&a, &b));
	printf("Overlap: %s\n", rtge_overlap_rect_rect(&a, &b) ? "True" : "False");
	rtge_2d_hit_info info;
	printf("Intersection: %s\n", rtge_intersection_rect_rect(&a, &b, &info) ? "True" : "False");
	printf("Penetration: %f\n", info.penetration);
	printf("Normal: (%f, %f)\n", info.normal.x, info.normal.y);

}
